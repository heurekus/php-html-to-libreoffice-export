<?php

/**
 * x-demo-writer-export.php
 *
 * This demo can be run directly from the shell. It converts and outputs
 * the HTML text given in 3 variables into a Libreoffice file.
 * 
 * Example shell command with XAMPP installed:
 * 
 *    /opt/lampp/bin/php x-demo-writer-export.php
 * 
 * Output file: 
 *  
 *    x-demo-writer-output.odt  --> Open with Libreoffice Writer
 *
 * IMPORTANT: This class requires the 'php-zip' module to be installed
 * which might have to be installed on the server separately from the
 * standard repositories (e.g. apt install php-zip)
 *
 * @version    1.1 2019-02-16
 * @package    libreoffice-export
 * @copyright  Copyright (c) 2018-2019 Martin Sauter
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

require_once 'lo-writer-export.php';

/* 
 * Simple HTML text, no formatting at all 
 */

$html_str = '<p>The FOX!</p>';


/*
 * Overlapping styles
 */

$html_str2 = '<p>The quick brown fox jumps over ' .
'<strong>the <span style="color:#ff0000">' .
'<span style="background-color:#00ffff">' .
'double colored dog.</span></span></strong><br></p>';


/* 
 * And text for a quotation with some formatting
 */

$html_str3 = '<p> Lorem ip<sup>sum</sup> dolor sit amet, ' . 
'consectetur âdipiscing elit. Aliquàm ut jüsto id mi suscipit ' . 
'commodo vel nec libero. Maecenas ' . 
'<span style="color:#ff0000">ullamcorper, ex at </span>' . 
'viverra pellentesque, nisl <strong><em>neque vulputate</strong> est, </em>' . 
'sed tincidunt metus enim ut metus.<br>' . 
'Cras ac magna blandit, vulputate nisi eu, maximus magna. ' . 
'Nullam aliquam porta velit eget fringilla.</p>';

// Create a writer export object to generate XML formatted text from
// a databas record and to hold style information.
$lo_export = new LO_Writer_Export();

// Now convert the HTML text to Libreoffice XML format. The result
// is stored in the object.
$log_buf = $lo_export->Convert_Html_To_LO_XML($html_str, "Heading 1", false);
$log_buf = $lo_export->Convert_Html_To_LO_XML($html_str2, "Standard", false);
$log_buf = $lo_export->Convert_Html_To_LO_XML($html_str3, "Quotations", false);

// Libreoffice template file
$src = 'x-demo-writer-template.odt';

// Output file, overwritten if it exists
$dst = 'x-demo-writer-output.odt';

// Now export the text to Libreoffice using the template and write
// the sum of the template and the text to a new file
$log_buf .= $lo_export->Save_To_Writer_Document($src, $dst);

// Output error debug messages if any.
var_dump($log_buf);


